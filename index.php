<?php

require __DIR__ . '/vendor/autoload.php';

$bookingValidator = new \App\Validator\BookingValidator();
$hotelValidator = new \App\Validator\HotelValidator();

$room1 = new \App\Model\Room(1, 1);
$room2 = new \App\Model\Room(2, 2);
$room3 = new \App\Model\Room(3, 1);

$rooms = [
    $room1,
    $room2,
    $room3,
];

$hotel = new \App\Model\Hotel($rooms, []);

$guest1 = new \App\Model\Guest();
$booking1 = new \App\Model\Booking(
    $guest1,
    $room1,
    100,
    new \DateTime('2018-01-01'),
    new \DateTime('2018-02-03')
);

$bookingValidator->validate($booking1);
$hotelValidator->validateBookingBeforeAdd($hotel, $booking1);

$hotel->addBooking($booking1);

$booking2 = new \App\Model\Booking(
    $guest1,
    $room1,
    100,
    new \DateTime('2018-02-01'),
    new \DateTime('2018-02-01')
);

$bookingValidator->validate($booking2);
$hotelValidator->validateBookingBeforeAdd($hotel, $booking2);
$hotel->addBooking($booking2);
