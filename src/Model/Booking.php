<?php

declare(strict_types=1);

namespace App\Model;

class Booking
{
    /**
     * @var Guest
     */
    private $guest;

    /**
     * @var Room
     */
    private $room;

    /**
     * @var float
     */
    private $cost;

    /**
     * @var \DateTimeInterface
     */
    private $startDate;

    /**
     * @var \DateTimeInterface
     */
    private $endDate;

    /**
     * @bean
     * @param Guest $guest
     * @param Room $room
     * @param float $cost
     * @param \DateTimeInterface $startDate
     * @param \DateTimeInterface $endDate
     */
    public function __construct(
        Guest $guest,
        Room $room,
        float $cost,
        \DateTimeInterface $startDate,
        \DateTimeInterface $endDate
    ) {
        $this->guest = $guest;
        $this->room = $room;
        $this->cost = $cost;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * @return Guest
     */
    public function getGuest(): Guest
    {
        return $this->guest;
    }

    /**
     * @return Room
     */
    public function getRoom(): Room
    {
        return $this->room;
    }

    /**
     * @return float
     */
    public function getCost(): float
    {
        return $this->cost;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getStartDate(): \DateTimeInterface
    {
        return $this->startDate;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getEndDate(): \DateTimeInterface
    {
        return $this->endDate;
    }
}