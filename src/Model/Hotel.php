<?php

declare(strict_types=1);

namespace App\Model;

class Hotel
{
    /**
     * @var array|Room[]
     */
    private $rooms;

    /**
     * @var array|Booking[]
     */
    private $bookings;

    /**
     * @bean
     * @param array|Room[] $rooms
     * @param array|Booking[] $bookings
     */
    public function __construct(array $rooms, array $bookings)
    {
        $this->rooms = $rooms;
        $this->bookings = $bookings;
    }

    /**
     * @return array|Room[]
     */
    public function getRooms(): array
    {
        return $this->rooms;
    }

    /**
     * @param Booking $booking
     * @return Hotel
     */
    public function addBooking(Booking $booking): self
    {
        $this->bookings[] = $booking;

        return $this;
    }

    /**
     * @return array|Booking[]
     */
    public function getBookings(): array
    {
        return $this->bookings;
    }

    /**
     * @param Room $room
     * @param \DateTimeInterface $date
     * @return bool
     */
    public function isRoomAvailableOnTheDate(Room $room, \DateTimeInterface $date): bool
    {
        foreach ($this->getBookings() as $booking) {
            if ((string) $booking->getRoom() !== (string) $room) {
                continue;
            }

            if ($date >= $booking->getStartDate()
                && $date <= $booking->getEndDate()
            ) {
                return false;
            }
        }

        return true;
    }
}