<?php

declare(strict_types=1);

namespace App\Model;

class Room
{
    /**
     * @var int
     */
    private $number;

    /**
     * @var int
     */
    private $places;

    /**
     * @bean
     * @param int $number
     * @param int $places
     */
    public function __construct(int $number, int $places)
    {
        $this->number = $number;
        $this->places = $places;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return 'Room #' . $this->getNumber();
    }

    /**
     * @return int
     */
    public function getNumber(): int
    {
        return $this->number;
    }

    /**
     * @return int
     */
    public function getPlaces(): int
    {
        return $this->places;
    }
}