<?php

declare(strict_types=1);

namespace App\Validator;

use App\Exception\ValidationException;
use App\Model\Booking;

class BookingValidator
{
    public function validate(Booking $booking)
    {
        if ($booking->getCost() <= 0) {
            throw new ValidationException('Cost must be larger than 0');
        }

        if ($booking->getEndDate() < $booking->getStartDate()) {
            throw new ValidationException('End date must be larger or equal to start date');
        }
    }
}