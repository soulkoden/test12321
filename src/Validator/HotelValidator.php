<?php

declare(strict_types=1);

namespace App\Validator;

use App\Exception\ValidationException;
use App\Model\{Booking, Hotel};

class HotelValidator
{
    public function validateBookingBeforeAdd(Hotel $hotel, Booking $booking)
    {
        if (!$hotel->isRoomAvailableOnTheDate($booking->getRoom(), $booking->getStartDate())
            || !$hotel->isRoomAvailableOnTheDate($booking->getRoom(), $booking->getEndDate())
        ) {
            throw new ValidationException('Dates are exists');
        }
    }
}