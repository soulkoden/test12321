<?php

declare(strict_types=1);

namespace Test\Functional;

use App\Exception\ValidationException;
use App\Model\{Booking, Guest, Hotel, Room};
use App\Validator\BookingValidator;
use App\Validator\HotelValidator;
use PHPUnit\Framework\TestCase;

class BookingTest extends TestCase
{
    /**
     * @dataProvider createSuccessData
     * @param Hotel $hotel
     * @param Booking[]|array $bookings
     */
    public function testBookingSuccess(Hotel $hotel, array $bookings)
    {
        $bookingValidator = new BookingValidator();
        $hotelValidator = new HotelValidator();

        foreach ($bookings as $booking) {
            $bookingValidator->validate($booking);
            $hotelValidator->validateBookingBeforeAdd($hotel, $booking);
            $hotel->addBooking($booking);
        }

        $this->assertCount(\count($bookings), $hotel->getBookings());
    }

    /**
     * @dataProvider createFailureData
     * @param Hotel $hotel
     * @param Booking[]|array $bookings
     */
    public function testBookingFailure(Hotel $hotel, array $bookings)
    {
        $bookingValidator = new BookingValidator();
        $hotelValidator = new HotelValidator();

        $this->expectException(ValidationException::class);

        foreach ($bookings as $booking) {
            $bookingValidator->validate($booking);
            $hotelValidator->validateBookingBeforeAdd($hotel, $booking);
            $hotel->addBooking($booking);
        }
    }

    /**
     * @return \Generator
     */
    public function createSuccessData()
    {
        $guest1 = new Guest();
        $guest2 = new Guest();
        $room1 = new Room(1, 1);
        $room2 = new Room(2, 2);
        $rooms = [$room1, $room2];

        yield [
            new Hotel($rooms, []),
            [
                new Booking($guest1, $room1, 100, new \DateTime('2019-01-01'), new \DateTime('2019-01-02')),
                new Booking($guest2, $room2, 80, new \DateTime('2019-01-01'), new \DateTime('2019-01-02')),
            ],
        ];

        yield [
            new Hotel($rooms, []),
            [
                new Booking($guest1, $room1, 100, new \DateTime('2019-01-01'), new \DateTime('2019-01-02')),
                new Booking($guest2, $room1, 80, new \DateTime('2019-01-03'), new \DateTime('2019-01-04')),
                new Booking($guest1, $room1, 120, new \DateTime('2019-01-05'), new \DateTime('2019-01-10')),
                new Booking($guest2, $room1, 140, new \DateTime('2019-01-11'), new \DateTime('2019-01-20')),
            ],
        ];
    }

    /**
     * @return \Generator
     */
    public function createFailureData()
    {
        $guest1 = new Guest();
        $guest2 = new Guest();
        $room1 = new Room(1, 1);
        $room2 = new Room(2, 2);
        $rooms = [$room1, $room2];

        yield [
            new Hotel($rooms, []),
            [
                new Booking($guest1, $room1, -10, new \DateTime('2029-01-01'), new \DateTime('2019-01-02')),
            ],
        ];

        yield [
            new Hotel($rooms, []),
            [
                new Booking($guest1, $room1, 100, new \DateTime('2029-01-01'), new \DateTime('2019-01-02')),
            ],
        ];

        yield [
            new Hotel($rooms, []),
            [
                new Booking($guest1, $room1, 100, new \DateTime('2019-01-01'), new \DateTime('2019-01-02')),
                new Booking($guest2, $room1, 80, new \DateTime('2019-01-01'), new \DateTime('2019-01-02')),
            ],
        ];
    }
}